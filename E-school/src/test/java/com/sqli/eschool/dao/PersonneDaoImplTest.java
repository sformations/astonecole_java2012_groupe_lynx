///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.sqli.eschool.dao;
//
//import com.sqli.eschool.modele.Personne;
//import com.sqli.eschool.technique.DaoException;
//import com.sqli.eschool.technique.UnanableToFormateDateException;
//import com.sqli.eschool.technique.Util;
//import java.util.Date;
//import java.util.List;
//import java.util.UUID;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import static org.junit.Assert.*;
//import org.junit.Before;
//import org.junit.Test;
//
///**
// *
// * @author Aris
// */
//public class PersonneDaoImplTest {
//
//    private PersonneDao dao;
//    String id = UUID.randomUUID().toString().substring(0, 6);
//    String email = UUID.randomUUID().toString().substring(0, 6)+"@eecole.com";
//
//    @Before
//    public void initilization() {
//        dao = new PersonneDaoImpl();
//    }
//
//    @Test
//    public void testCreerPersonne() {
//        try {
//
//            Date date = Util.createDate("25-11-2012", "dd-MM-yyyy");
//            Personne personne = new Personne(id, "Jean", "Brice", date, email,
//                    "ddfdfb", "dddf", "dfdff", "dfqdf", "cccf", "44400", "ADMIN", "ACTIF");
//
//            Personne pr = dao.creerPersonne(personne);
//            assertNotNull(pr);
//            System.out.println(pr.getDateNaissance());
//        } catch (UnanableToFormateDateException ex) {
//            Logger.getLogger(PersonneDaoImplTest.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (DaoException ex) {
//            Logger.getLogger(PersonneDaoImplTest.class.getName()).log(
//                    Level.SEVERE, null, ex);
//        }
//    }
//	@Test
//	public void testChercherPersonneParId() {
//
//		try {
//			Personne personne = dao.chercherUnePersonneParId("4ab913");
//			assertNotNull(personne);
//			
//		}catch (DaoException e) {
//			e.getMessage();
//		}catch (Exception ex) {
//			ex.getMessage();
//		}
//	}
//	
//	@Test
//	public void testModifierPersonne() {
//		Personne personne = new Personne("4ab913", "Jean", "Brice", new Date(), "jack bauer",
//				"ddfdfb", "dddf", "dfdff", "dfqdf", "cccf", "44400","ADMIN","ACTIF");
//		try {
//			Personne personne1 = dao.modifierPersonne(personne);
//			assertNotNull(personne1);
//			
//		}catch (DaoException e) {
//			e.getMessage();
//		}catch (Exception ex) {
//			ex.getMessage();
//		}
//	}
//	
//	@Test
//	public void testListerLesPersonne(){
//		try {
//			List<Personne> personne = dao.listerLesPersonnes();
//			assertTrue(personne.size()>=1);
//			
//		}catch (DaoException e) {
//			e.getMessage();
//		}catch (Exception ex) {
//			ex.getMessage();
//		}
//	}
//	
//        
//        @Test
//	public void testChercherPersonneParLoginMotDePass() {
//
//		try {
//			Personne personne = dao.chercherUnePersonneParLoginMotPass("4ab913","dddf");
//                        System.out.println(personne.getMotDePasse());
//			assertTrue("dddf", "ddfdfb".equals(personne.getLogin()));
//                        
//                        
//			
//		}catch (DaoException e) {
//			e.getMessage();
//		}catch (Exception ex) {
//			ex.getMessage();
//		}
//	}
//        
//        @Test
//	public void testChercherPersonne(){
//		try {
//			List<Personne> personne = dao.chercherPersonne("ENSEIGNANT");
//			assertTrue(personne.size()>=1);
//			System.out.println(personne.size());
//		}catch (DaoException e) {
//			e.getMessage();
//		}catch (Exception ex) {
//			ex.getMessage();
//		}
//	}
//                
//}
