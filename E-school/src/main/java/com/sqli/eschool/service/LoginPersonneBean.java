/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sqli.eschool.service;

import com.sqli.eschool.dao.PersonneDao;
import com.sqli.eschool.dao.PersonneDaoImpl;
import com.sqli.eschool.modele.Personne;
import com.sqli.eschool.technique.DaoException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

/**
 *
 * @author Administrateur
 */
public class LoginPersonneBean {

    /**
     * Creates a new instance of LoginPersonneBean
     */
    private String login;
    private String motDePasse;
    private String typePersonne;
    
            
    private PersonneDao dao ;
    
    @PostConstruct
    public void initialization() {        
        this.dao = new PersonneDaoImpl();   
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public String getTypePersonne() {
        return typePersonne;
    }

    public void setTypePersonne(String typePersonne) {
        this.typePersonne = typePersonne;
    }
    
    public String checkLoging(){  
        try {
            Personne myUser = dao.chercherUnePersonneParLoginMotPass(login, motDePasse);
            
            if ("ETUDIANT".equals(myUser.getTypePersonne())){
                 typePersonne = "ETUDIANT";   
                 System.out.println(typePersonne);
            }
            else if ("ADMIN".equals(myUser.getTypePersonne())){                
                 typePersonne = "ADMIN";         
                 System.out.println(typePersonne);
            }
            else if ("ENSEIGNANT".equals(myUser.getTypePersonne())){
                 typePersonne = "ENSEIGNANT";  
                 System.out.println(typePersonne);
            }        
            else {
                 typePersonne = "NOSUCCESS"; 
                 
            }
                 
        } catch (DaoException ex) {
            Logger.getLogger(LoginPersonneBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception e){
            e.getMessage();
        }
        System.out.println(typePersonne);
        
        return typePersonne ;
    }    
    
        public String disconnect() {
        if(typePersonne == null) {
            throw new IllegalStateException("Cannot disconnect an unidentified user");
        }
        ((javax.servlet.http.HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false)).invalidate();
        return "HOME";
    }
        
}
