/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sqli.eschool.service;

import com.sqli.eschool.dao.PersonneDao;
import com.sqli.eschool.dao.PersonneDaoImpl;
import com.sqli.eschool.modele.Personne;
import com.sqli.eschool.technique.DaoException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

/**
 *
 * @author Administrateur
 */
public class ChercherAdminsBean {

    /**
     * Creates a new instance of LoginPersonneBean
     */
    private PersonneDao dao;
    private List<Personne> listePersonne;
    private String typePersonne="ADMIN";
    
    @PostConstruct
    public void initioalization(){
        try {
            this.dao=new PersonneDaoImpl();
            listePersonne=dao.chercherPersonne(typePersonne);
        } catch (DaoException ex) {
            ex.getMessage();
        }
    }

    public List<Personne> getListePersonne() {
         System.out.println(listePersonne.size());
        return listePersonne;
    }

    public void setListePersonne(List<Personne> listePersonne) {
        this.listePersonne = listePersonne;
    }

    public String getTypePersonne() {
        return typePersonne;
       
    }

    public void setTypePersonne(String typePersonne) {
        this.typePersonne = typePersonne;
    }
      
    
    
}
