/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sqli.eschool.service;

import com.sqli.eschool.dao.PersonneDao;
import com.sqli.eschool.dao.PersonneDaoImpl;
import com.sqli.eschool.modele.Personne;
import com.sqli.eschool.technique.DaoException;
import java.util.List;
import javax.annotation.PostConstruct;

/**
 *
 * @author Administrateur
 */
public class ChercherEnseignantsBean {

    /**
     * Creates a new instance of LoginPersonneBean
     */
    private PersonneDao dao;
    private List<Personne> listePersonne;
    private String typePersonne="ENSEIGNANT";
    
    @PostConstruct
    public void initioalization(){
        try {
            this.dao=new PersonneDaoImpl();
            listePersonne=dao.chercherPersonne(typePersonne);
        } catch (DaoException ex) {
            ex.getMessage();
        }
    }

    public List<Personne> getListePersonne() {
         System.out.println(listePersonne.size());
        return listePersonne;
    }

    public void setListePersonne(List<Personne> listePersonne) {
        this.listePersonne = listePersonne;
    }

    public String getTypePersonne() {
        return typePersonne;
       
    }

    public void setTypePersonne(String typePersonne) {
        this.typePersonne = typePersonne;
    }
      
    
    
}
