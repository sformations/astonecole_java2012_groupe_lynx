/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sqli.eschool.modele;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Administrateur
 */
public class Promotion implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = -1793330125746256787L;
	private String id;
    private String libelle;
    private List<Etudiant> listeEtudiant;
    private List<Cours> listeCours;

    public Promotion(String libelle, List<Etudiant> listeEtudiant, List<Cours> listeCours) {
        this.libelle = libelle;
        this.listeEtudiant = listeEtudiant;
        this.listeCours = listeCours;
    }

    public String getId() {
        return id;
    }

    public String getLibelle() {
        return libelle;
    }

    public List<Etudiant> getListeEtudiant() {
        return listeEtudiant;
    }

    public List<Cours> getListeCours() {
        return listeCours;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public void setListeEtudiant(List<Etudiant> listeEtudiant) {
        this.listeEtudiant = listeEtudiant;
    }

    public void setListeCours(List<Cours> listeCours) {
        this.listeCours = listeCours;
    }
    
    
    
}
