/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sqli.eschool.modele;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 *
 * @author Administrateur
 */
public class Cours implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 8740358473179431514L;
	private String id;
    private Date dateCours;
    private Date dureeEnMinute;
    private String description;
    private List<Absence> absences;
    private Promotion promotion;
    private Matiere matiere;

    public Cours(Date dateCours, Date dureeEnMinute, String description) {
        this.dateCours = dateCours;
        this.dureeEnMinute = dureeEnMinute;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    
    
    public void setDateCours(Date dateCours) {
        this.dateCours = dateCours;
    }

    public void setDureeEnMinute(Date dureeEnMinute) {
        this.dureeEnMinute = dureeEnMinute;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateCours() {
        return dateCours;
    }

    public Date getDureeEnMinute() {
        return dureeEnMinute;
    }

    public String getDescription() {
        return description;
    }

    public Promotion getPromotion() {
        return promotion;
    }

    public void setPromotion(com.sqli.eschool.modele.Promotion promotion) {
        this.promotion = promotion;
    }

    public Matiere getMatiere() {
        return matiere;
    }

    public void setMatiere(Matiere matiere) {
        this.matiere = matiere;
    }

    public List<Absence> getAbsences() {
        return absences;
    }

    public void setAbsences(List<Absence> absences) {
        this.absences = absences;
    }
    
   
    
    
    
}
