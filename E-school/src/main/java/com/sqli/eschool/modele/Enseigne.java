/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sqli.eschool.modele;

import java.io.Serializable;




/**
 *
 * @author Administrateur
 */
public class Enseigne implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 4279466289766421192L;
	private String id;
    private Matiere matiere;
    private Promotion promotion;

    public Enseigne(Matiere matiere, Promotion promotion) {
        this.matiere = matiere;
        this.promotion = promotion;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    
    
    
    public Matiere getMatiere() {
        return matiere;
    }

    public Promotion getPromotion() {
        return promotion;
    }

    public void setMatiere(Matiere matiere) {
        this.matiere = matiere;
    }

    public void setPromotion(Promotion promotion) {
        this.promotion = promotion;
    }
    
    
    
}
