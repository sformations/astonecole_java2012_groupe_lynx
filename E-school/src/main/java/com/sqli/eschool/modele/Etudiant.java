/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sqli.eschool.modele;

import java.util.Date;


/**
 *
 * @author Administrateur
 */
public class Etudiant extends Personne {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1041148728076663860L;
	private Date dateDeNaissance ;

    public Etudiant(Date dateDeNaissance,String id, String nom, String prenom, String email, String photo, String rue, String codePostal, String ville, String login, String motDePasse) {
        super(id, nom, prenom, dateDeNaissance, email, login, motDePasse, photo, rue, codePostal, ville, motDePasse, rue);
        this.dateDeNaissance = dateDeNaissance;
    }
    

    public Date getDateDeNaissance() {
        return dateDeNaissance;
    }

    public void setDateDeNaissance(Date dateDeNaissance) {
        this.dateDeNaissance = dateDeNaissance;
    }
   
    
    
}
