/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sqli.eschool.modele;

import java.util.List;

/**
 *
 * @author Administrateur
 */
public class Enseignant extends Personne {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1498139242877688913L;
	private List<Enseigne> enseignne;

    public Enseignant(List<Enseigne> enseignne,String id, String nom, String prenom, String email, String photo, String rue, String codePostal, String ville, String login, String motDePasse) {
        super(id, nom, prenom, null, email, login, motDePasse, photo, rue, codePostal, ville, motDePasse, rue);
        this.enseignne = enseignne;
    }
    

    public List<Enseigne> getEnseignne() {
        return enseignne;
    }

    public void setEnseignne(List<Enseigne> enseignne) {
        this.enseignne = enseignne;
    }
    
    
}
