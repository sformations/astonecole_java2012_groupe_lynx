/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sqli.eschool.modele;

import java.io.Serializable;

/**
 *
 * @author Administrateur
 */
public class Matiere implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 8066484620415147512L;
	private String id;
    private String nom;
    private Cours cours;
    
    

    public Matiere(String nom, Cours cours) {
        this.nom = nom;
        this.cours = cours;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    
    public String getNom() {
        return nom;
    }

    public Cours getCours() {
        return cours;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setCours(Cours cours) {
        this.cours = cours;
    }
    
    
    
    
    
    
}
