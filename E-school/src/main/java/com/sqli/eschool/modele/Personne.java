/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sqli.eschool.modele;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Administrateur
 */
public class Personne implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 767140658377656058L;
    private String id;
    private String nom;
    private String prenom;
    private Date dateNaissance;
    private String email;
    private String login;
    private String motDePasse;
    private String photo;
    private String rue;
    private String codePostal;
    private String ville;
    private String typePersonne;
    private String statut;

    public Personne(String id, String nom, String prenom, Date dateNaissance, String email, String login, String motDePasse, String photo, String rue, String codePostal, String ville, String typePersonne, String statut) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.dateNaissance = dateNaissance;
        this.email = email;
        this.login = login;
        this.motDePasse = motDePasse;
        this.photo = photo;
        this.rue = rue;
        this.codePostal = codePostal;
        this.ville = ville;
        this.typePersonne = typePersonne;
        this.statut = statut;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getTypePersonne() {
        return typePersonne;
    }

    public void setTypePersonne(String typePersonne) {
        this.typePersonne = typePersonne;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    

    // private Adresse adresse;
    public String getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoto() {
        return photo;
    }

    public String getRue() {
        return rue;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public String getVille() {
        return ville;
    }

    public String getLogin() {
        return login;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
