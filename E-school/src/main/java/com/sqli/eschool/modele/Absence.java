
package com.sqli.eschool.modele;

import java.io.Serializable;


public class Absence implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = -6594400286155873772L;
	private String id;
    private String absenceId;
    private String motif;
    //private int etudiantId;

    public Absence() {
    }

    public Absence(String absenceId, String motif) {
        this.absenceId = absenceId;
        this.motif = motif;
    }

    public String getAbsenceId() {
        return absenceId;
    }

    public void setAbsenceId(String absenceId) {
        this.absenceId = absenceId;
    }

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    
}
