package com.sqli.eschool.dao;

import com.sqli.eschool.modele.Personne;
import com.sqli.eschool.technique.DaoException;
import java.util.List;

/**
 *
 * @author Aris
 */
public interface PersonneDao {
  public Personne creerPersonne(Personne personne) throws DaoException;
  public Personne modifierPersonne(Personne personne) throws DaoException;
  public Personne chercherUnePersonneParId(String id) throws DaoException;
  public List<Personne> chercherPersonne(String typePersonne) throws DaoException;
  public Personne chercherUnePersonneParLoginMotPass(String login,String motDePass) throws DaoException;
  public List<Personne> listerLesPersonnes() throws DaoException;
  public void supprimerPersonne(Personne personne) throws DaoException;  
}
