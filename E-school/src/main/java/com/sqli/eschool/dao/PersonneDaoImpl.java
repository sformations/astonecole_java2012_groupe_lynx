/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sqli.eschool.dao;
import com.sqli.eschool.modele.Personne;
import com.sqli.eschool.technique.DaoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;



/**
 *
 * @author Aris
 */

public class PersonneDaoImpl extends ConnexioPersonneJDBC implements PersonneDao {

	private Connection connection;
	private Statement statement;
    
    @Override
    public Personne creerPersonne(Personne personne) throws DaoException {
        try {
            //Ouverture de la connection
            connection=getConnexionJDBC();
            // Construction de la requête
            StringBuilder query = new StringBuilder();
            query.append("INSERT INTO personne(PERSONNE_ID,NOM,PRENOM,DATE_NAISSANCE,EMAIL,LOGIN,MOTDEPASS,PHOTO,RUE,VILLE,CODEPOSTAL,TYPE_PERSONNE,STATUT) VALUES ");
            query.append("(");
            query.append("'").append(personne.getId()).append("'");
            query.append(",");
            query.append("'").append(personne.getNom()).append("'");
            query.append(",");
            query.append("'").append(personne.getPrenom()).append("'");
            query.append(",");
            java.sql.Date dsql = new java.sql.Date(personne.getDateNaissance().getTime());
            query.append("'").append(dsql).append("'");
            query.append(",");
            query.append("'").append(personne.getEmail()).append("'");
            query.append(",");
            query.append("'").append(personne.getLogin()).append("'");
            query.append(",");
            query.append("'").append(personne.getMotDePasse()).append("'");
            query.append(",");
            query.append("'").append(personne.getPhoto()).append("'");
            query.append(",");
            query.append("'").append(personne.getRue()).append("'");
            query.append(",");
            query.append("'").append(personne.getVille()).append("'");
            query.append(",");
            query.append("'").append(personne.getCodePostal()).append("'");
            query.append(",");
            query.append("'").append(personne.getTypePersonne()).append("'");
            query.append(",");
            query.append("'").append(personne.getStatut()).append("'");
            query.append(")");
            
            System.out.println(query);
            
          // Création du statement
           statement = connection.createStatement();

           // Exécution de la requête
	   statement.executeUpdate(query.toString());
       

		
    
        } catch (SQLException ex) {
            Logger.getLogger(PersonneDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
        	
        	// Fermeture de la connection
    		if (connection != null) {
    			try {
					connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    		}
    		
    		//fermeture de l'unité de traitement
    		 if (statement != null) {
    				try {
						statement.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
    			}
            
        }
        return personne;
    }
    
    
    

    @Override
    public Personne modifierPersonne(Personne personne) throws DaoException {
    	
 
    	PreparedStatement st = null;
		Connection con =null;
		try {
			con = getConnexionJDBC();
			st = con.prepareStatement("UPDATE personne SET NOM=?,PRENOM=?,EMAIL=?,LOGIN=?,MOTDEPASS=?,PHOTO=?, RUE=?,VILLE=?," +
					"CODEPOSTAL=? WHERE PERSONNE_ID=?");
			st.setString(1, personne.getNom());
			st.setString(2, personne.getPrenom());
			st.setString(3, personne.getEmail());
			st.setString(4, personne.getLogin());
			st.setString(5, personne.getMotDePasse());
			st.setString(6, personne.getPhoto());
			st.setString(7, personne.getRue());
			st.setString(8, personne.getVille());
			st.setString(9, personne.getCodePostal());
			st.setString(10,personne.getId());
			int cmp = st.executeUpdate();
			if(cmp==0){
				throw new DaoException("Unable update produit:"+personne.getNom());
			}
			
		} catch (SQLException e) {
			try {
				if (con!=null) {
					con.rollback();
				}
			} catch (SQLException e2) {
				throw new DaoException(e2.getMessage(),e2);
			}catch (Exception ex) {
				throw new DaoException(ex.getMessage(),ex);
			}
		}finally{
			try {
				if (con!=null) {
					con.close();
				}
				if (st!=null) {
					st.close();
				}
			} catch (SQLException e3) {
				throw new DaoException(e3.getMessage(),e3);
			}
			
		}
       return personne;
    }

    
    
    @Override
    public List<Personne> listerLesPersonnes() throws DaoException {
        List<Personne> listes=new  ArrayList<Personne>();
        Statement st = null;
		Connection con = null;
		ResultSet rs = null;
		try {
			con = getConnexionJDBC();
			st = con.createStatement();
			rs = st.executeQuery("SELECT * FROM personne");
			while(rs.next()){
				Personne pers = new Personne(rs.getString(1),rs.getString(2),rs.getString(3),rs.getDate(4),rs.getString(5),rs.getString(6),rs.getString(7),rs.getString(8),rs.getString(9),rs.getString(10),rs.getString(11),rs.getString(12),rs.getString(13));
				listes.add(pers);
			}
			
		} catch (SQLException e) {
			throw new DaoException(e.getMessage(),e);
		}finally{
			try {
				if (con!=null) {
					con.close();
				}
				if (st!=null) {
					st.close();
				}
				if (rs!=null) {
					rs.close();
				}
			} catch (SQLException e2) {
				throw new DaoException(e2.getMessage(),e2);
			}
		}
        return listes;
    }

    @Override
    public void supprimerPersonne(Personne personne) throws DaoException {
    }




	@Override
	public Personne chercherUnePersonneParId(String id) throws DaoException {
		Personne personne=null;
		try {

			// Récupération de la connexion
			Connection connection = getConnexionJDBC();

			// Construction de la requête
			StringBuilder query = new StringBuilder();
			query.append("SELECT * FROM PERSONNE WHERE ");
			query.append("PERSONNE_ID='"+id+"'");

			// Création du statement
			Statement statement = connection.createStatement();

			// Exécution de la requête
			ResultSet resultSet = statement.executeQuery(query.toString());

			// Récupération des résultats
			if (resultSet != null) {
				while (resultSet.next()) {
					String idp = resultSet.getString("PERSONNE_ID");
					String nom = resultSet.getString("NOM");
					String prenom = resultSet.getString("PRENOM");
                                        Date dateNaissance = resultSet.getDate("DATE_NAISSANCE");                                        
					String email = resultSet.getString("EMAIL");
					String login = resultSet.getString("LOGIN");
					String motDePasse = resultSet.getString("MOTDEPASS");
					String photo = resultSet.getString("PHOTO");
					String rue = resultSet.getString("RUE");
					String ville = resultSet.getString("VILLE");
					String codePostal = resultSet.getString("CODEPOSTAL");
                                        String typePersonne = resultSet.getString("TYPE_PERSONNE");
                                        String statut = resultSet.getString("STATUT");
					personne = new Personne(idp, nom, prenom, dateNaissance, email, login, motDePasse, photo, rue, ville, codePostal,typePersonne,statut);
				}
			}

			// Fermeture du statement
			if (statement != null) {
				statement.close();
			}

			// Fermeture de la connection
			if (connection != null) {
				connection.close();

			}
		} catch (Exception e) {
			// TODO: handle exception
		
			e.printStackTrace();
			e.getMessage();
		}
	    return personne;
	}

    @Override
    public Personne chercherUnePersonneParLoginMotPass(String login, String motDePass) throws DaoException {
       Personne personne=null;
		try {

			// Récupération de la connexion
			Connection connection = getConnexionJDBC();

//                      Construction de la requête
			StringBuilder query = new StringBuilder();
			query.append("SELECT * FROM PERSONNE WHERE ");
			query.append("LOGIN='"+login+"'");
                        query.append("AND MOTDEPASS='"+motDePass+"'");
                        //System.out.println(query);
			// Création du statement
			Statement statement = connection.createStatement();

			// Exécution de la requête
			//ResultSet resultSet = statement.executeQuery("SELECT * FROM PERSONNE WHERE LOGIN='"+login+"'"+"AND MOTDEPASS='"+motDePass+"'");
                        ResultSet resultSet = statement.executeQuery(query.toString());
			// Récupération des résultats
			if (resultSet != null) {
				while (resultSet.next()) {
					String idp = resultSet.getString("PERSONNE_ID");
					String nom = resultSet.getString("NOM");
					String prenom = resultSet.getString("PRENOM");
                                        Date dateNaissance = resultSet.getDate("DATE_NAISSANCE");                                        
					String email = resultSet.getString("EMAIL");
					String identifiant = resultSet.getString("LOGIN");
					String motDePasse = resultSet.getString("MOTDEPASS");
					String photo = resultSet.getString("PHOTO");
					String rue = resultSet.getString("RUE");
					String ville = resultSet.getString("VILLE");
					String codePostal = resultSet.getString("CODEPOSTAL");
                                        String typePersonne = resultSet.getString("TYPE_PERSONNE");
                                        String statut = resultSet.getString("STATUT");
					personne = new Personne(idp, nom, prenom, dateNaissance, email, identifiant, motDePasse, photo, rue, ville, codePostal,typePersonne,statut);
				}
			}

			// Fermeture du statement
			if (statement != null) {
				statement.close();
			}

			// Fermeture de la connection
			if (connection != null) {
				connection.close();

			}
		} catch (Exception e) {
			// TODO: handle exception
		
			e.printStackTrace();
			e.getMessage();
		}
	    return personne;
    }

    @Override
    public List<Personne> chercherPersonne(String typePersonne) throws DaoException {
        
        List<Personne> listes=new  ArrayList<Personne>();
        Statement st = null;
		Connection con = null;
		ResultSet rs = null;
		try {
			con = getConnexionJDBC();
			st = con.createStatement();
			rs = st.executeQuery("SELECT * FROM personne WHERE TYPE_PERSONNE='"+typePersonne+"' ");
                        System.out.println(typePersonne);
			while(rs.next()){
				Personne pers = new Personne(rs.getString(1),rs.getString(2),rs.getString(3),rs.getDate(4),rs.getString(5),rs.getString(6),rs.getString(7),rs.getString(8),rs.getString(9),rs.getString(10),rs.getString(11),rs.getString(12),rs.getString(13));
				listes.add(pers);
			}
			
		} catch (SQLException e) {
			throw new DaoException(e.getMessage(),e);
		}finally{
			try {
				if (con!=null) {
					con.close();
				}
				if (st!=null) {
					st.close();
				}
				if (rs!=null) {
					rs.close();
				}
			} catch (SQLException e2) {
				throw new DaoException(e2.getMessage(),e2);
			}
		}
        return listes;                
    }


    
}
