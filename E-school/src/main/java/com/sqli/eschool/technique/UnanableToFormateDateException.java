package com.sqli.eschool.technique;



public class UnanableToFormateDateException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2857898044922900631L;
	public UnanableToFormateDateException(String message, Throwable cause){
		super(message,cause);
	}

}
