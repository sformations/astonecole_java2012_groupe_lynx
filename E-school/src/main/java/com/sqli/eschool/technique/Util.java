package com.sqli.eschool.technique;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Util  {
	private static final String DATE_FORMAT ="dd/MM/yyyy";
	
	
	public static Date createDate(String dateFormat) throws UnanableToFormateDateException{
		DateFormat format = new SimpleDateFormat(DATE_FORMAT);
		Date d;
		try {
			d = format.parse(dateFormat);
		} catch (ParseException e) {
			throw new UnanableToFormateDateException(e.getMessage(), e);
		}
		return  d;
	}
	
	public static Date createDate(String dateFormat, String pattern) throws UnanableToFormateDateException{
		DateFormat format = new SimpleDateFormat(pattern);
		Date d;
		try {
			d = format.parse(dateFormat);
		} catch (ParseException e) {
			throw new UnanableToFormateDateException(e.getMessage(), e);
		}
		return  d;
	}
	
}
