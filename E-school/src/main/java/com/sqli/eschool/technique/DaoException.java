/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sqli.eschool.technique;

/**
 *
 * @author Aris
 */
public class DaoException extends Exception{
        /**
	 * 
	 */
	private static final long serialVersionUID = -2271118689473501926L;

		public DaoException(String message){
		super(message);
	}
	
	public DaoException(String message, Throwable ex){
		super(message, ex);
	}
    
}
